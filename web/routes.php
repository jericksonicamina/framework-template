<?php

Router::set(
    [
        ['POST', '/auth-user', 'ApiLoginController@authenticateUser'],
        ['POST', '/get-sample-data', 'ApiLoginController@getSampleData'],
        ['GET', '/', 'HomeController@Homie'],
        ['GET', '/home', 'HomeController@Index'],
        ['GET', '/homes', 'HomeController@Homie'],
        ['GET', '/remove', 'HomeController@Remove'],
    ]
);