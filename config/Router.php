<?php

class Router {
    
    public static function set($params) {
        $valid_routes = Array();
        $current_url =  self::getCurrentURLName();
        foreach($params as $data) {
            array_push($valid_routes, $data[1]);
        }

        
        // check if supplied url is valid
        if (in_array($current_url, $valid_routes)) {
            foreach($params as $data) {
                // match the supplied url to declared route
                if ($data[1] === $current_url) {
                    if ($_SERVER['REQUEST_METHOD'] === $data[0]) {
                        $controller_name = explode('@', $data[2]);
                        $classname = $controller_name[0];
                        $newClass = new $classname();
                        $funcName = $controller_name[1];
                        $newClass->$funcName();
                    } else {
                        echo "Request Method Mistmatch";
                    }
                }
            }
        } else {
            $controller = new Controller();
            $controller->createView('errors.404');
        }
    }

    private static function getCurrentURLName() {
        $current_url = explode('index.php', $_SERVER['REQUEST_URI']);
        $current_index = sizeof($current_url);
        return $current_url[($current_index - 1)];
    }
}