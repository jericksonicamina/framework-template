<?php

class Database {

    public $host;
    public $dbName;
    public $username;
    public $password;

    private function connect() {
        $pdo = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->dbName . ";charset=UTF8", $this->username, $this->password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    public function query($stmt , $params = array()) {
        $query = $this->connect()->prepare($stmt);
        $query->execute($params);
        if (explode(' ', $stmt)[0] === 'SELECT') {
            $query->setFetchMode(PDO::FETCH_ASSOC); 
            return $query->fetchAll();
        }
    }
}