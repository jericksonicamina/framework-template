<?php

class HomeController extends Controller {
    
    public function Index() {
        $_SESSION['firstname'] = 'Jerick';
        echo 'in home index ' . $_SESSION['firstname'];
    }

    public function Homie() {
        echo $_SESSION['firstname'];
    }

    public function Remove() {
        unset($_SESSION['firstname']);
    }
}