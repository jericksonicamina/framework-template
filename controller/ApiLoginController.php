<?php
header('Content-Type: application/json; Charset=UTF-8');

class ApiLoginController extends ApiController{
    
    public function authenticateUser() {
        $payload = $this->getPayload();
        echo $this->buildJsonResponse("Success", 'authenticateUser', $payload);
    }

    public function getSampleData() {
        $stmt = "SELECT * FROM `test_tbl` WHERE id = :id";
        $params = Array(':id' => 1);
        $data = $this->query($stmt, $params);
        echo $this->buildJsonResponse("Success", "getSampleData", $data);
    }
}