<?php

class ApiController extends Database{
    
    public $host = '127.0.0.1';
    public $dbName = 'tester';
    public $username = 'root';
    public $password = '145769823';

    public function getPayload() {
        return json_decode(file_get_contents('php://input'));
    }

    public function buildJsonResponse($type, $message, $data = []) {
        /*
         * The type ('Success', 'Failed', 'Error') and message parameters must be string
         * while data must be an array
         */
        $response = [
            'status' => $type,
            'message' => $type . ': ' . $message,
        ];
    
        if ($type == 'Success' || $type == 'Warning') {
            $response['payload'] = $data;
        }
        return json_encode($response);
    }

    /**
     * Format the date to match MYSQL compliance.
     * @param $date
     * @return false|string
     */
    public function formatDate($date) {
        return date_format(date_create($date),'Y-m-d H:m:s');
    }
}