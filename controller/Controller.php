<?php
class Controller {

    public function createView($name) {
        $split_name = explode('.', $name);
        $name_size = sizeof($split_name);
        $filename = './views';
        for ($i = 0; $i < $name_size; $i++) {
            $filename .= (($i + 1) == $name_size) ? '/' . $split_name[$i] . '.php' : '/' . $split_name[$i];
        }
        
        if (file_exists($filename)) {
            require_once($filename);
        } else {
            require_once('./views/errors/404.php');
        }
    }
}