<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
session_save_path(__DIR__);
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
spl_autoload_register(function($classname) {
    if (file_exists('./config/' . $classname . '.php')) {
        require_once './config/' . $classname . '.php';
    } else if (file_exists('./controller/' . $classname . '.php')) {
        require_once './controller/' . $classname . '.php';
    }
});

require_once('./web/routes.php');